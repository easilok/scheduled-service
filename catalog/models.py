from django.db import models
from django.contrib.auth.models import AbstractUser
from django.utils.translation import ugettext_lazy as _
from django.core.validators import MinLengthValidator

from .managers import CustomUserManager, UserDetailManager,\
    EstablishmentCategoryManager, EstablishmentManager,\
    ServiceManager

class User(AbstractUser):
    username = None
    email = models.EmailField(_('email address'), unique=True)

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['password']

    objects = CustomUserManager()

    def __str__(self):
        return self.email

class UserDetail(models.Model):
    user = models.OneToOneField(
        User,
        on_delete = models.CASCADE,
        primary_key = True,
        unique=True,
        null=False
    )
    first_name = models.CharField(max_length=20, null=False)
    last_name = models.CharField(max_length=20, null=False)
    taxnumber = models.CharField(max_length=20, blank=True, null=True)
    address = models.CharField(max_length=50, blank=True, null=True)
    city = models.CharField(max_length=50, blank=True, null=True)
    postal = models.CharField(max_length=10, blank=True, null=True)
    tel1 = models.CharField(max_length=15, blank=True, null=True)
    tel2 = models.CharField(max_length=15, blank=True, null=True)
    photo = models.CharField(max_length=50, blank=True, null=True)
    obs = models.CharField(max_length=50, blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    objects = UserDetailManager()

    def __str__(self):
        return self.name() + ": " + self.email()

    def email(self):
        return self.user.email

    def name(self):
        return self.first_name + " " + self.last_name

class EstablishmentCategory(models.Model):
    name = models.CharField(max_length=20, validators=[MinLengthValidator(2, message=_('Name too small'))])
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    objects = EstablishmentCategoryManager()

    def __str__(self):
        return self.name

class Establishment(models.Model):
    name = models.CharField(max_length=20, validators=[MinLengthValidator(2, message=_('Name too small'))])
    taxnumber = models.CharField(max_length=20, blank=True, null=True)
    address = models.CharField(max_length=50, blank=True, null=True)
    city = models.CharField(max_length=50, blank=True, null=True)
    postal = models.CharField(max_length=10, blank=True, null=True)
    tel1 = models.CharField(max_length=15, blank=True, null=True)
    tel2 = models.CharField(max_length=15, blank=True, null=True)
    email = models.EmailField(max_length=50, blank=True, null=True)
    latitude = models.DecimalField(max_digits=12, decimal_places=9, blank=True, null=True)
    longitude = models.DecimalField(max_digits=12, decimal_places=9, blank=True, null=True)
    website = models.CharField(max_length=100, blank=True, null=True)
    active = models.BooleanField(default=True)

    PREFERED_CONTACT = (
        ( 0, 'telephone1'),
        ( 1, 'telephone2'),
        ( 2, 'email'),
    )
    pref_contact = models.IntegerField(
        choices=PREFERED_CONTACT,
        blank=True,
        default=0,
        help_text=_('Prefered Contact')
    )

    PREFERED_LOCALIZATION = (
        ( 0, 'name'),
        ( 1, 'address'),
        ( 2, 'gps'),
    )
    pref_map = models.IntegerField(
        choices=PREFERED_LOCALIZATION,
        blank=True,
        default=0,
        help_text=_('Prefered Localization')
    )

    owner = models.ForeignKey('User', on_delete=models.SET_NULL, null=True)
    opentime = models.CharField(max_length=255, blank=True, null=True)
    category = models.ForeignKey('EstablishmentCategory', on_delete=models.SET_NULL, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    objects = EstablishmentManager()

    def __str__(self):
        return self.name

class Service(models.Model):
    name = models.CharField(max_length=20, validators=[MinLengthValidator(2, message=_('Name too small'))])
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    objects = ServiceManager()

    def __str__(self):
        return self.name

class EstablishmentService(models.Model):
    establishment = models.ForeignKey('Establishment', on_delete=models.CASCADE)
    service = models.ForeignKey('Service', on_delete=models.CASCADE)
    localname = models.CharField(max_length=20, blank=True, null=True)
    duration = models.DecimalField(max_digits=6, decimal_places=2, default=0.0)
    cost = models.DecimalField(max_digits=7, decimal_places=2, default=0.0)
    workinghours = models.CharField(max_length=255, blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    # objects = EstablishmentServiceManager()

    class Meta:
        unique_together = (('establishment', 'service'),)

    def name(self):
        if self.localname != None:
            return self.localname + " (" + self.service.name + ")"
        return self.service.name

    def __str__(self):
        return self.name()

class Employee(models.Model):
    establishment = models.ForeignKey('Establishment', on_delete=models.CASCADE)
    user = models.ForeignKey('User', on_delete=models.CASCADE)
    workinghours = models.CharField(max_length=255, blank=True, null=True)
    salary = models.DecimalField(max_digits=7, decimal_places=2, default=0.0)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        unique_together = (('establishment', 'user'),)

    def name(self):
        return self.user.email + " (" + self.establishment.name + ")"

    def __str__(self):
        return self.name()


class EmployeeService(models.Model):
    employee = models.ForeignKey('Employee', on_delete=models.CASCADE)
    service = models.ForeignKey('EstablishmentService', on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        unique_together = (('employee', 'service'),)

    def name(self):
        return self.employee.name() + " (" + self.service.name() + ")"

    def __str__(self):
        return self.name()

# class EstablishmentConfig(models.Model):
#     pass

