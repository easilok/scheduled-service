from django.test import TestCase
from django.contrib.auth import get_user_model
from django.db import models
from django.db.utils import IntegrityError

from .models import UserDetail, EstablishmentCategory,\
    Establishment, Service, EstablishmentService,\
    Employee, EmployeeService

class UsersManagersTests(TestCase):

    def test_create_user(self):
        # Check user creation and default variables
        User = get_user_model()
        user = User.objects.create_user(email='normal@user.com', password='foo')
        self.assertEqual(user.email, 'normal@user.com')
        self.assertTrue(user.is_active)
        self.assertFalse(user.is_staff)
        self.assertFalse(user.is_superuser)
        try:
            # username is None for the AbstractUser option
            # username does not exist for the AbstractBaseUser option
            self.assertIsNone(user.username)
        except AttributeError:
            pass

    def test_create_empty_user(self):
        # Check if a empty user cannot be created
        User = get_user_model()
        with self.assertRaises(TypeError):
            User.objects.create_user()

    def test_create_empty_email(self):
        # Check if a user with empty email cannot be created
        User = get_user_model()
        with self.assertRaises(TypeError):
            User.objects.create_user(email='')

    def test_create_email_with_no_password(self):
        # Check if a user with no password cannot be created
        User = get_user_model()
        with self.assertRaises(ValueError):
            User.objects.create_user(email='normal@user.com', password="")

    def test_create_empty_email_with_password(self):
        # Check if a user with password and empty email cannot be created
        User = get_user_model()
        with self.assertRaises(ValueError):
            User.objects.create_user(email='', password="foo")

    def test_create_superuser(self):
        # Check super user creation and default variables
        User = get_user_model()
        admin_user = User.objects.create_superuser('super@user.com', 'foo')
        self.assertEqual(admin_user.email, 'super@user.com')
        self.assertTrue(admin_user.is_active)
        self.assertTrue(admin_user.is_staff)
        self.assertTrue(admin_user.is_superuser)
        try:
            # username is None for the AbstractUser option
            # username does not exist for the AbstractBaseUser option
            self.assertIsNone(admin_user.username)
        except AttributeError:
            pass

    def test_create_superuser_with_superuser_to_false(self):
        # Check if a superuser with not is_superuser cannot be created
        User = get_user_model()
        with self.assertRaises(ValueError):
            User.objects.create_superuser(
                email='super@user.com', password='foo', is_superuser=False)


class UserDetailsManagersTests(TestCase):
    def setUp(self):
        User = get_user_model()
        User.objects.create_user(email='user1@user.com', password='foo')
        User.objects.create_user(email='user2@user.com', password='foo')

    def test_create_userdetail_with_only_name(self):
        User = get_user_model()
        user1 = User.objects.all()[0]
        fName = 'foo'
        lName = 'bar'
        userDetail = UserDetail.objects.create(user = user1, first_name=fName, last_name=lName);
        self.assertEqual(userDetail.email(), user1.email)
        self.assertEqual(userDetail.first_name, fName)
        self.assertEqual(userDetail.last_name, lName)
        self.assertEqual(userDetail.name(), fName + " " + lName)

    def test_create_userdetail_with_no_user(self):
        User = get_user_model()
        fName = 'foo'
        lName = 'bar'
        with self.assertRaises(TypeError):
            userDetail = UserDetail.objects.create(first_name=fName, last_name=lName);

    def test_create_userdetail_with_no_name(self):
        User = get_user_model()
        user1 = User.objects.all()[0]
        fName = 'foo'
        lName = 'bar'
        with self.assertRaises(TypeError):
            userDetail = UserDetail.objects.create(user=user1);

    def test_create_userdetail_with_only_first_a_name(self):
        User = get_user_model()
        user1 = User.objects.all()[0]
        fName = 'foo'
        lName = 'bar'
        with self.assertRaises(TypeError):
            userDetail = UserDetail.objects.create(user=user1, first_name=fName);

    def test_create_userdetail_with_only_last_a_name(self):
        User = get_user_model()
        user1 = User.objects.all()[0]
        fName = 'foo'
        lName = 'bar'
        with self.assertRaises(TypeError):
            userDetail = UserDetail.objects.create(user=user1, last_name=lName);

    def test_userdetail_delete_when_user_delete(self):
        User = get_user_model()
        user1 = User.objects.all()[0]
        fName = 'foo'
        lName = 'bar'
        userDetail = UserDetail.objects.create(user = user1, first_name=fName, last_name=lName);
        user1.delete()
        with self.assertRaises(UserDetail.DoesNotExist):
            newUserDetail = UserDetail.objects.get(pk=userDetail.pk)

class EstablishmentCategoriesManagersTests(TestCase):
    def test_establishmentcategory_create(self):
        catName = 'Category'
        cat = EstablishmentCategory.objects.create(name=catName);
        self.assertEqual(cat.name, catName)

    def test_establishmentcategory_create_empty(self):
        with self.assertRaises(TypeError):
            cat = EstablishmentCategory.objects.create();

    def test_establishmentcategory_create_empty_name(self):
        with self.assertRaises(ValueError):
            cat = EstablishmentCategory.objects.create(name='');

class EstablishmentManagersTests(TestCase):
    def setUp(self):
        User = get_user_model()
        User.objects.create_user(email='user1@user.com', password='foo')
        User.objects.create_user(email='user2@user.com', password='foo')
        EstablishmentCategory.objects.create(name='category')

    def test_establishment_create_only_name(self):
        estName = 'Establishment'
        est = Establishment.objects.create(name=estName);
        self.assertEqual(est.name, estName)
        self.assertEqual(est.owner, None)
        self.assertEqual(est.category, None)
        self.assertTrue(est.active)

    def test_establishment_create_name_user(self):
        estName = 'Establishment'
        User = get_user_model()
        user1 = User.objects.all()[0]
        user2 = User.objects.all()[1]
        est = Establishment.objects.create(name=estName, owner=user1);
        self.assertEqual(est.name, estName)
        self.assertTrue(est.active)
        self.assertEqual(est.owner.email, user1.email)
        self.assertEqual(est.category, None)

    def test_establishment_create_name_category(self):
        estName = 'Establishment'
        cat1 = EstablishmentCategory.objects.all()[0]
        est = Establishment.objects.create(name=estName, category=cat1);
        self.assertEqual(est.name, estName)
        self.assertTrue(est.active)
        self.assertEqual(est.category.pk, cat1.pk)
        self.assertEqual(est.owner, None)

    def test_establishment_create_empty(self):
        with self.assertRaises(TypeError):
            cat = Establishment.objects.create();

    def test_establishment_create_empty_name(self):
        with self.assertRaises(ValueError):
            cat = Establishment.objects.create(name='');

class ServicesManagersTests(TestCase):
    def test_service_create(self):
        testName = 'Service'
        objectModel = Service.objects.create(name=testName);
        self.assertEqual(objectModel.name, testName)

    def test_service_create_empty(self):
        with self.assertRaises(TypeError):
            objectModel = Service.objects.create();

    def test_service_create_empty_name(self):
        with self.assertRaises(ValueError):
            objectModel = Service.objects.create(name='');

class EstablishmentServicesTests(TestCase):
    def setUp(self):
        Establishment.objects.create(name='Establishment 1')
        Service.objects.create(name='Service 1')

    def test_establishmentservice_create(self):
        est = Establishment.objects.all()[0]
        service = Service.objects.all()[0]
        estService = EstablishmentService.objects.create(establishment = est, service = service)
        self.assertEqual(estService.establishment.pk, est.pk)
        self.assertEqual(estService.service.pk, service.pk)

    def test_establishmentservice_default_service_name(self):
        est = Establishment.objects.all()[0]
        service = Service.objects.all()[0]
        estService = EstablishmentService.objects.create(establishment = est, service = service)
        self.assertEqual(estService.name(), service.name)

    def test_establishmentservice_localname(self):
        est = Establishment.objects.all()[0]
        service = Service.objects.all()[0]
        localName = "Local Service 1"
        estService = EstablishmentService.objects.create(establishment = est, service = service, localname=localName)
        self.assertEqual(estService.name(), localName + " (" + service.name + ")")

    def test_establishmentservice_create_empty(self):
        est = Establishment.objects.all()[0]
        service = Service.objects.all()[0]
        with self.assertRaises(IntegrityError):
            estService = EstablishmentService.objects.create()

    def test_establishmentservice_create_only_establishment(self):
        est = Establishment.objects.all()[0]
        service = Service.objects.all()[0]
        with self.assertRaises(IntegrityError):
            estService = EstablishmentService.objects.create(establishment=est)

    def test_establishmentservice_create_only_service(self):
        est = Establishment.objects.all()[0]
        service = Service.objects.all()[0]
        with self.assertRaises(IntegrityError):
            estService = EstablishmentService.objects.create(service=service)

class EmployeesTests(TestCase):
    def setUp(self):
        Establishment.objects.create(name='Establishment 1')
        User = get_user_model()
        user = User.objects.create_user(email='normal@user.com', password='foo')

    def test_employee_create(self):
        est = Establishment.objects.all()[0]
        User = get_user_model()
        user = User.objects.all()[0]
        employee = Employee.objects.create(establishment = est, user = user)
        self.assertEqual(employee.establishment.pk, est.pk)
        self.assertEqual(employee.user.pk, user.pk)

    def test_employee_create_empty(self):
        est = Establishment.objects.all()[0]
        User = get_user_model()
        user = User.objects.all()[0]
        with self.assertRaises(IntegrityError):
            employee = Employee.objects.create()

    def test_employee_create_only_establishment(self):
        est = Establishment.objects.all()[0]
        User = get_user_model()
        user = User.objects.all()[0]
        with self.assertRaises(IntegrityError):
            employee = Employee.objects.create(establishment = est)

    def test_employee_create_only_service(self):
        est = Establishment.objects.all()[0]
        User = get_user_model()
        user = User.objects.all()[0]
        with self.assertRaises(IntegrityError):
            employee = Employee.objects.create(user = user)

class EmployeeServicesTests(TestCase):
    def setUp(self):
        est = Establishment.objects.create(name='Establishment 1')
        User = get_user_model()
        user = User.objects.create_user(email='normal@user.com', password='foo')
        Employee.objects.create(user=user, establishment=est)
        serv = Service.objects.create(name='Service 1')
        EstablishmentService.objects.create(service=serv, establishment=est)

    def test_employee_service_create(self):
        employee = Employee.objects.all()[0]
        service = EstablishmentService.objects.all()[0]
        employeeServ = EmployeeService.objects.create(employee=employee, service=service)
        self.assertEqual(employeeServ.employee.pk, employee.pk)
        self.assertEqual(employeeServ.service.pk, service.pk)

    def test_employee_service_create_empty(self):
        employee = Employee.objects.all()[0]
        service = EstablishmentService.objects.all()[0]
        with self.assertRaises(IntegrityError):
            employeeServ = EmployeeService.objects.create()

    def test_employee_service_create_only_employee(self):
        employee = Employee.objects.all()[0]
        service = EstablishmentService.objects.all()[0]
        with self.assertRaises(IntegrityError):
            employeeServ = EmployeeService.objects.create(employee=employee)

    def test_employee_service_create_only_service(self):
        employee = Employee.objects.all()[0]
        service = EstablishmentService.objects.all()[0]
        with self.assertRaises(IntegrityError):
            employeeServ = EmployeeService.objects.create(service=service)
