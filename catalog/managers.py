from django.contrib.auth.base_user import BaseUserManager
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth import get_user_model
from django.db import models


class CustomUserManager(BaseUserManager):
    """
    Custom user model manager where email is the unique identifiers
    for authentication instead of usernames.
    """
    def create_user(self, email, password, **extra_fields):
        """
        Create and save a User with the given email and password.
        """
        if not email:
            raise ValueError(_('The Email must be set'))
        if not password:
            raise ValueError(_('You must set a password'))
        email = self.normalize_email(email)
        user = self.model(email=email, **extra_fields)
        user.set_password(password)
        user.save()
        return user

    def create_superuser(self, email, password, **extra_fields):
        """
        Create and save a SuperUser with the given email and password.
        """
        extra_fields.setdefault('is_staff', True)
        extra_fields.setdefault('is_superuser', True)
        extra_fields.setdefault('is_active', True)

        if extra_fields.get('is_staff') is not True:
            raise ValueError(_('Superuser must have is_staff=True.'))
        if extra_fields.get('is_superuser') is not True:
            raise ValueError(_('Superuser must have is_superuser=True.'))
        return self.create_user(email, password, **extra_fields)

class UserDetailManager(models.Manager):
    """
    Custom userdetail model manager where
    where user and name is required
    """
    def create(self, user, first_name, last_name, **extra_fields):
        User = get_user_model()
        if not type(user) is User:
            raise ValueError(_('Invalid User'))
        if not first_name:
            raise ValueError(_('Invalid name'))
        if not last_name:
            raise ValueError(_('Invalid name'))

        userdetail = self.model(user=user, first_name=first_name, last_name=last_name, **extra_fields)
        userdetail.save()
        return userdetail

class EstablishmentCategoryManager(models.Manager):
    """
    Custom EstablishmentCategory model manager where
    name is required
    """
    def create(self, name, **extra_fields):
        if not name:
            raise ValueError(_('Invalid name'))

        category = self.model(name=name, **extra_fields)
        category.save()
        return category

class EstablishmentManager(models.Manager):
    """
    Custom Establishment model manager where
    name is required
    """
    def create(self, name, **extra_fields):
        if not name:
            raise ValueError(_('Invalid name'))

        establishment = self.model(name=name, **extra_fields)
        establishment.save()
        return establishment

class ServiceManager(models.Manager):
    """
    Custom Service model manager where
    name is required
    """
    def create(self, name, **extra_fields):
        if not name:
            raise ValueError(_('Invalid name'))

        service = self.model(name=name, **extra_fields)
        service.save()
        return service

