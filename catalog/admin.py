from django.contrib import admin
from django.contrib.auth.admin import UserAdmin

from .models import User, UserDetail,\
    EstablishmentCategory, Establishment,\
    EstablishmentService, Service

# Custom admin classes
class CustomUserAdmin(UserAdmin):
    # add_form = CustomUserCreationForm
    # form = CustomUserChangeForm
    model = User
    list_display = ('email', 'is_staff', 'is_active', 'date_joined',)
    list_filter = ('email', 'is_staff', 'is_active', 'date_joined',)
    fieldsets = (
        (None, {'fields': ('email', 'password', )}),
        ('Permissions', {'fields': ('is_staff', 'is_active')}),
    )
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('email', 'password1', 'password2', 'is_staff', 'is_active')}
        ),
    )
    search_fields = ('email',)
    ordering = ('email',)

admin.site.register(User, CustomUserAdmin)
admin.site.register(UserDetail)
admin.site.register(EstablishmentCategory)
admin.site.register(Establishment)
admin.site.register(Service)
admin.site.register(EstablishmentService)
