## About Service Schedule

 This is a plataform build with the purpose of allow any establishment that supplies any kind of services by schedule. It will allow
 the customers to make an appointment on the free time slots, and rate the service at the end. For the establishments, it will allow
 to make an history of what was done to each client for future reference, with observations and photographs of the work. The platform
 was intruduced to help hairdresser but can be used with any kind of appointment service. The specification
 can be found in [this file](https://gitlab.com/easilok/scheduled-service/-/blob/master/docs/specification/specification.pdf).

